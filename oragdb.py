import cx_Oracle, gdb, sys, json

class OB(gdb.Breakpoint):
    def __init__(self, bname, sid, sqlStats, curStat):
        gdb.Breakpoint.__init__(self, bname)
        self.count = 0
        self.sid = sid
        self.sqlStats = sqlStats
        self.statList = []
        self.statVal = {}
        self.curStat = curStat

    def stop(self):
        self.count += 1
        caller = gdb.newest_frame()
        caller_name = caller.name() if caller else 'none'
	result = self.curStat.execute(self.sqlStats, [self.sid,]).fetchall()
        statVal = {}
	for stats in result:
            statVal[stats[0]] = stats[1]

        self.statList.append(statVal)
        self.statList = self.statList[-2:]

        if len(self.statList) == 2:
            print(caller_name + " = " + str(self.count))
            print(gdb.execute("bt", to_string=True))
            for key in statVal:
                if self.statList[0].get(key, 0) != self.statList[1].get(key, 0):
		    print("\t" + key + " = " + str(self.statList[1].get(key, 0) - self.statList[0].get(key, 0)))

        return False


if __name__ == "__main__":
    with open("oragdb.conf") as f:
        params = json.load(f)

    conn_obj = cx_Oracle.connect(params["dbstring"], mode=cx_Oracle.SYSDBA)
    brks = params["breakpoints"]
    breakpointObjects = []
    sqlGetSid = """select sid
                        from v$session s, v$process p
                        where s.paddr=p.addr
                        and   p.spid=:1
                """
    sqlStats = params["SQL"]
    cur = conn_obj.cursor()
    sid = cur.execute(sqlGetSid, [gdb.selected_inferior().pid,]).fetchall()[0][0]
    for brk in brks:
        breakpointObjects.append(OB(brk, sid, sqlStats, cur))
